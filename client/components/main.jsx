import React from 'react';
import First from './first';
import Second from './second';
import Users from './users';
import User from './user';
import { Link } from 'react-router';

export default class Main extends React.Component {
	componentDidMount() {
		console.log(this.props.children);
	}
	render() {
		return (
			<div>
				<li><Link to="/first">First</Link></li>
				<li><Link to="/second">Second</Link></li>
				<li><Link to="/users">Users</Link></li>
				<li><Link to="/todoapp">TodoApp</Link></li>
				{this.props.children}
			</div>
		);
	}
}
