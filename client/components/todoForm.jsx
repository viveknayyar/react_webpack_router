import React from 'react';
import ReactDom from 'react-dom';
import AutoComplete from './autocomplete';
export default class TodoForm extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			item: ''
		};
	}
	handleSubmit(e) {
		e.preventDefault();
		this.props.onSubmit(ReactDom.findDOMNode(this.refs.comment).value);
		this.setState({
			item: ''
		});
	}
	handleChange() {
		let current_text = ReactDom.findDOMNode(this.refs.comment).value;
		this.setState({
			item: current_text
		});

	}
	render() {
		return (
			<form name='comment_form' className='comment_form'>
				<AutoComplete />
				<button className='submit' onClick={(e)=>this.handleSubmit(e)}>Submit Comment</button>
			</form>
		);
	}
}
