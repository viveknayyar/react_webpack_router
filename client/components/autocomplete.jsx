import React from 'react';
import $ from 'jquery';
import Typeahead from 'typeahead.js';
let results = [];

export default class AutoComplete extends React.Component {
	constructor(props) {
		super(props);
		this.initializeTypeahead();
	}
	componentDidMount() {
		this.bindTypeahead();
	}
	initializeTypeahead() {
		results = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			limit: 10,
			remote: {
				url: 'https://buy.housing.com/api/v0/search/suggest/?&string=%QUERY',
				filter: function (parsedResponse) {
					var results =  parsedResponse.map(function(item) {
						return item;
					});
					console.log(results);
					return results;
				},
				wildcard: '%QUERY'
			}
		});
		results.initialize();
	}
	bindTypeahead() {
		$('.typeahead').typeahead({
			minLength: 1,
			highlight: true,
			hint: true,
		},{
			source: results.ttAdapter(),
			displayKey: 'name',
			templates: {
				empty: [
					'<div class="noitems">',
					'No Items Found',
					'</div>'
				].join('\n'),
				suggestion: function(data){
					if(data.type=='city') {
						return (
							[
								// '<img src=' + data.image_url + ' />',
								'<p>' + data.name + '<strong> - ' + data.class + '</strong></p>'
							].join('\n')
						);
					} else {
						return (
							'<p>' + data.name + '<strong> - ' + data.class + '</strong></p>'
						);
					}
				},
			},
		});
	}
	render() {
		return (
			<input className='typeahead' id='autocomplete' />
		);
	}
}
