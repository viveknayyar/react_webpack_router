import React from 'react';
import TodoItem from './todoItem';
export default class TodoList extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		let items = this.props.items.map((item,index) =>
			<TodoItem data={item} key={index} />
		);
		return (
			<div>
				{items}
			</div>
		);
	}
}
