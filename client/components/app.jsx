import React from 'react';
import TodoBanner from './todo_banner';
import TodoList from './todoList';
import ReactDom from 'react-dom';
import TodoForm from './todoForm';
export default class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			items: ["Hi how are you","I am fine thank you"]
		};
	}
	handleSubmit(item) {
		let items = this.state.items.concat([item]);
		this.setState({
			items: items
		});
	}
	render() {
		return (
			<div className='container'>
				<TodoBanner />
				<TodoForm onSubmit={(item)=>this.handleSubmit(item)} />
				<TodoList items={this.state.items} />
			</div>
		);
	}
}
// ReactDom.render(<App />, document.getElementById('root'));

