import React from 'react';
import Main from './components/main';
import { render } from 'react-dom';
import { Router, Route, Link, browserHistory } from 'react-router';
import First from './components/first';
import Second from './components/second';
import Users from './components/users';
import User from './components/user';
import TodoApp from './components/app';
render((
	<Router history={browserHistory}>
		<Route path="/" component={Main}>
			<Route path="first" component={First}/>
			<Route path="second" component={Second}/>
			<Route path="users" component={Users}>
				<Route path=":id" component={User}/>
			</Route>
			<Route path="todoapp" component={TodoApp}/>
		</Route>
	</Router>
	),document.getElementById('root')
);
