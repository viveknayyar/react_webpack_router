var webpack = require('webpack');
var path = require('path');

module.exports = {
	context: __dirname + '/client',
	entry: [
		'./router'
	],
	output: {
		path: __dirname + '/build',
		filename: 'bundle.js',
	},
	debug: true,
	devtool: "#inline-source-map",
	resolve: {
		extensions: ['', '.jsx','.js']
	},
	module: {
		loaders: [{
			test: /\.jsx?$/,
			loaders: ['react-hot','babel'],
			exclude: /node_modules/
		},
		{
			test: /\.json$/,
			loader: "json-loader" // reference by npm module or path to loader file
		}]
	},
};
